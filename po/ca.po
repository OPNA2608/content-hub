# Catalan translation for content-hub
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the content-hub package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: content-hub\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-05 15:40+0000\n"
"PO-Revision-Date: 2024-04-23 12:06+0000\n"
"Last-Translator: Joan CiberSheep <cibersheep@gmail.com>\n"
"Language-Team: Catalan <https://hosted.weblate.org/projects/lomiri/"
"content-hub/ca/>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.5-dev\n"
"X-Launchpad-Export-Date: 2016-12-01 04:56+0000\n"

#: src/com/lomiri/content/detail/service.cpp:238
msgid "Download Complete"
msgstr "S'ha acabat la baixada"

#: src/com/lomiri/content/detail/service.cpp:256
msgid "Open"
msgstr "Obre"

#: src/com/lomiri/content/detail/service.cpp:263
msgid "Dismiss"
msgstr "Descarta"

#: src/com/lomiri/content/detail/service.cpp:281
msgid "Download Failed"
msgstr "Ha fallat la baixada"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Choose from"
msgstr "Trieu entre"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Open with"
msgstr "Obre amb"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Share to"
msgstr "Comparteix a"

#: import/Lomiri/Content/ContentPeerPicker10.qml:162
#: import/Lomiri/Content/ContentPeerPicker13.qml:170
#: import/Lomiri/Content/ContentPeerPicker11.qml:180
msgid "Apps"
msgstr "Aplicacions"

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can provide this type "
"of content."
msgstr ""
"No hi ha cap aplicació instal·lada que proporcioni aquest tipus de contingut."

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can handle this type "
"of content."
msgstr ""
"No hi ha cap aplicació instal·lada que gestioni aquest tipus de contingut."

#: import/Lomiri/Content/ContentPeerPicker10.qml:214
#: import/Lomiri/Content/ContentPeerPicker13.qml:223
#: import/Lomiri/Content/ContentPeerPicker11.qml:232
msgid "Devices"
msgstr "Dispositius"

#: import/Lomiri/Content/ContentPeerPicker10.qml:251
#: import/Lomiri/Content/ContentPeerPicker11.qml:59
#: import/Lomiri/Content/ContentTransferHint.qml:65
msgid "Cancel"
msgstr "Cancel·la"

#: import/Lomiri/Content/ContentTransferHint.qml:52
msgid "Transfer in progress"
msgstr "Transferència en curs"

#: examples/picker-qml/picker.qml:21
msgid "Peer Picker Example"
msgstr "Exemple de selecció d'origen"

#: examples/picker-qml/picker.qml:37
msgid "Sources"
msgstr "Fonts"

#: examples/picker-qml/picker.qml:39
msgid "Select source"
msgstr "Selecciona la font"

#: examples/picker-qml/picker.qml:52
msgid "Results"
msgstr "Resultats"
