/*
 * Copyright 2013 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../../src/com/lomiri/content/debug.h"
#include "contentpeer.h"
#include "contentstore.h"
#include "contenttype.h"

/*!
 * \qmltype ContentStore
 * \instantiates ContentStore
 * \inqmlmodule Lomiri.Content
 * \brief Sets the type of location that content should be transferred to
 *
 * A ContentStore allows for the permanent storage of a transfered item.
 *
 * See documentation for ContentHub and ContentScope
 */

namespace cuc = com::lomiri::content;

ContentStore::ContentStore(QObject *parent)
    : QObject(parent),
      m_scope(ContentScope::System)
{
    TRACE() << Q_FUNC_INFO;
}

/*!
 * \qmlproperty uri ContentStore::uri
 *
 * URI of the content store
 */
const QString &ContentStore::uri() const
{
    TRACE() << Q_FUNC_INFO;

    return m_uri;
}

/*!
 * \brief ContentStore::setUri
 * \internal
 */
void ContentStore::setUri(const QString & uri)
{
    m_uri = uri;

    Q_EMIT uriChanged();
}

/*!
 * \qmlproperty ContentScope ContentStore::scope
 *
 * Specifies the ContentScope for this store.
 */
ContentScope::Scope ContentStore::scope()
{   
    TRACE() << Q_FUNC_INFO;
    return m_scope;
}

/*!
 * \brief ContentStore::setScope
 * \internal
 */
void ContentStore::setScope(ContentScope::Scope scope)
{   
    TRACE() << Q_FUNC_INFO;
    m_scope = scope;

    Q_EMIT scopeChanged();
}
